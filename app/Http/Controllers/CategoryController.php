<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CategoryController extends Controller
{
    public function index()
    {
        $category = DB::table('categories')->get();
        return view('fontend/layouts/category/index', compact('category'));
    }

    public function create()
    {
        return view('fontend/layouts/category/create');
    }

    public function store(Request $request)
    {
        $data = array();
        $data['category_name'] = $request->get('category_name');

        DB::table('categories')->insert($data);
        return redirect('index-category');
    }

    public function view($id)
    {
        $view = DB::table('categories')->where('id', $id)->first();
        return view('fontend/layouts/category/view', compact('view'));
    }

    public function edit($id)
    {
        $edit = DB::table('categories')->where('id', $id)->first();
        return view('fontend/layouts/category/edit', compact('edit'));
    }

    public function update(Request $request, $id)
    {
        $data = array();
        $data['category_name'] = $request->get('category_name');

        $data = DB::table('categories')->where('id', $id)->update($data);
        return redirect('index-category');
    }

     public function delete($id)
     {
        DB::table('categories')->where('id', $id)->delete();
        return redirect('index-category');
     }

}
