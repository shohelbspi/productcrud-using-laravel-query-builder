<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;;
use DB;

class ProductController extends Controller
{
    public function  index()
    {
        $product = DB::table('products')
            ->join('categories', 'products.cat_id', '=', 'categories.id')
            ->join('subcategories', 'products.subcat_id', '=', 'subcategories.id')
            ->select('products.*', 'categories.category_name','subcategories.subcategory_name')
            ->get();
            // dd($product);
        
        return view('fontend/layouts/products/index', compact('product'));
    }

    public function  create()
    {
        $category = DB::table('categories')->get();
        $subcategory = DB::table('subcategories')->get();
        return view('fontend/layouts/products/create', compact('category', 'subcategory'));
    }

    
    public function  store(Request $request)
    {
        $data = array();
        $data['product_name'] = $request->get('product_name');
        $data['title'] = $request->get('title');
        $data['price'] = $request->get('price');
        $data['cat_id'] = $request->get('cat_id');
        $data['subcat_id'] = $request->get('subcat_id');
        $data['description'] = $request->get('description');

        DB::table('products')->insert($data);
        return redirect('index-Product');
    }

    public function  view($id)
    {
        $data = DB::table('products')
        ->join('categories', 'products.cat_id', '=', 'categories.id')
        ->join('subcategories', 'products.subcat_id', '=', 'subcategories.id')
        -> where('products.id', $id)
        // -> where('categories.id', $id)
        ->select('products.*', 'categories.category_name','subcategories.subcategory_name')
        ->first();
        // dd($data);
        return view('fontend/layouts/products/view', compact('data'));
    }

    public function  edit($id)
    {
        $category = DB::table('categories')->get();
        $subcategory = DB::table('subcategories')->get();
        $edit = DB::table('products')->where('id', $id)->first();
        return view('fontend/layouts/products/edit', compact('category','subcategory','edit'));
    }

    public function  update(Request $request,$id)
    {
        $data['product_name'] = $request->get('product_name');
        $data['title'] = $request->get('title');
        $data['price'] = $request->get('price');
        $data['cat_id'] = $request->get('cat_id');
        $data['subcat_id'] = $request->get('subcat_id');
        $data['description'] = $request->get('description');
        $data = DB::table('products')->where('id',$id)->update($data);
        return redirect('index-Product');
    }

    public function  delete($id)
    {
        DB::table('products')->where('id', $id)->delete();
        return redirect('index-Product');
    }
}

