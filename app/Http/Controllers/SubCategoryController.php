<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SubCategoryController extends Controller
{
    public function index()
    {
        $subcategory = DB::table('subcategories')
            ->join('categories', 'subcategories.cat_id', '=', 'categories.id')
            ->select('subcategories.*', 'categories.category_name')
            ->get();
        return view('fontend/layouts/subcategory/index', compact('subcategory'));
    }

    public function create()
    {
        $data = DB::table('categories')->get();
        return view('fontend/layouts/subcategory/create', compact('data'));
    }

    public function store(Request $request)
    {
        $data = array();
        $data['subcategory_name'] = $request->subcategory_name;
        $data['cat_id'] = $request->cat_id;
        DB::table('subcategories')->insert($data);
        return redirect('index-subcategory');
    }

    public function view($id)
    {
        $data = DB::table('subcategories')
        ->join('categories', 'subcategories.cat_id', '=', 'categories.id')
        -> where('subcategories.id', $id)
        ->select('subcategories.*', 'categories.category_name')
        ->first();
        
        return view('fontend/layouts/subcategory/view', compact('data'));
    }

    public function edit($id)

    {
        $edit = DB::table('subcategories')->where('id', $id)->first();
        $data = DB::table('categories')->get();
        return view('fontend/layouts/subcategory/edit',compact('edit','data'));
    }

    public function update(Request $request,$id)
    {
        $data['subcategory_name'] = $request->subcategory_name;
        $data['cat_id'] = $request->cat_id;
        $data = DB::table('subcategories')->where('id',$id)->update($data);
        
        return redirect('index-subcategory');
    }

    public function delete($id)
    {
        
        $data = DB::table('subcategories')->where('id', $id)->delete();
        return redirect('index-subcategory');

    }
}
