@extends('fontend/layouts/master')

@section('title')
    Categoty View
@endsection


@section('bodyContent')
<div class="container my-4">
    <div class="card" style="width: 30%">
        <div class="card-header">
            <a href="{{url ('index-category')}}"><button style="float: right" class="btn btn-success">All Category</button></a>
            Category Data
        </div>
        <div class="card-body">
            <p>Category Name: {{$view->category_name}}</p>
        </div>
    </div>
</div>

@endsection