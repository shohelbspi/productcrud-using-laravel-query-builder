@extends('fontend/layouts/master')

@section('title')
Categoty Create
@endsection


@section('bodyContent')
<div class="container my-4">
    <div class="card">
        <div class="card-header">
            <a href="{{url('index-category')}}"><button  type="button" style="float: right" class="btn btn-success">All Category</button></a>
            <h2>Create Category</h2>
        </div>
        <div class="card-body">
            <form method="POST", action="{{url('store-category')}}">
                @csrf
                <div class="mb-3">
                    <label for="category_name" class="form-label">Category Name</label>
                    <input type="text" class="form-control" id="category_name" name="category_name"
                        aria-describedby="CategoryName">
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection