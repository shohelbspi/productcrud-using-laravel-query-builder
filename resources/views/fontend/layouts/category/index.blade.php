@extends('fontend/layouts/master')

@section('title')
Categoty Index
@endsection


@section('bodyContent')
<div class="container my-4">
    <div class="card">
        <div class="card-header">
            <a href="{{url ('create-category')}}"><button style="float: right" class="btn btn-success">Add
                    Category</button></a>
            <h2>All Category</h2>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">SL</th>
                        <th scope="col">Name</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>



                <tbody>

                    @foreach ($category as $item)
                    {{-- @php
                        $sl = 0
                    @endphp
                    <tr> --}}
                    <tr>
                        <td>{{ $item->id}}</td>
                        <td>{{$item->category_name}}</td>
                        <td class='mx-2'>
                            <a href="{{url ('view-category/'.$item->id)}}"><button
                                    class="btn btn-success">View</button></a>
                            <a href="{{url ('edit-category/'.$item->id)}}"><button
                                    class="btn btn-primary">Edit</button></a>
                            <a href="{{url ('delete-category/'.$item->id)}}"><button
                                    class="btn btn-danger">Delete</button></a>
                        </td>
                    </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection