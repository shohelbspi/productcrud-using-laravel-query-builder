@extends('fontend/layouts/master')

@section('title')
Product Create
@endsection


@section('bodyContent')
<div class="container my-4">
    <div class="card">
        <div class="card-header">
            <a href="{{url('index-Product')}}"><button  type="button" style="float: right" class="btn btn-success">All Product</button></a>
            <h2>Create product</h2>
        </div>
        <div class="card-body">
            <form method="POST", action="{{url('store-Product')}}">
                @csrf
                <div class="mb-3">
                    <label for="product_name" class="form-label">Product Name</label>
                    <input type="text" class="form-control" id="product_name" name="product_name"
                        aria-describedby="product_name">
                </div>

                <div class="mb-3">
                    <label for="title" class="form-label">Title</label>
                    <input type="text" class="form-control" id="title" name="title"
                        aria-describedby="title">
                </div>

                <div class="mb-3">
                    <label for="price" class="form-label">Price</label>
                    <input type="text" class="form-control" id="price" name="price"
                        aria-describedby="price">
                </div>

                <div class="mb-3">
                    <select class="form-select" aria-label="Default select example" name="cat_id">
                        @foreach ($category as $item)
                            
                        <option value="{{$item->id}}">{{$item->category_name}}</option>
                        @endforeach
        
                      </select>
                </div>

                <div class="mb-3">
                    <select class="form-select" aria-label="Default select example" name="subcat_id">
                        @foreach ($subcategory as $items)
                            
                        <option value="{{$items->id}}">{{$items->subcategory_name}}</option>
                        @endforeach
        
                      </select>
                </div>

                <div class="mb-3">
                    <label for="description" class="form-label">Description</label>
                    <input type="text" class="form-control" id="description" name="description"
                        aria-describedby="description">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection