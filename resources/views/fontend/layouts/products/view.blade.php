@extends('fontend/layouts/master')

@section('title')
Products view
@endsection

@section('bodyContent')
<div class="container my-4">
    <div class="card" style="width: 30%">
        <div class="card-header">
            <a href="{{url ('index-Product')}}"><button style="float: right" class="btn btn-success">All Product</button></a>
            Product Data
        </div>
        <div class="card-body">
            <p>Product Name : {{$data->product_name}}</p>
            <p>Title: {{$data->title}}</p>
            <p>Price : {{$data->price}}</p>
            <p>Category Name : {{$data->category_name}}</p>
            <p>Sub Category Name: {{$data->subcategory_name}}</p>
            <p>Sub Category Name: {{$data->description}}</p>
        </div>
    </div>
</div>
@endsection