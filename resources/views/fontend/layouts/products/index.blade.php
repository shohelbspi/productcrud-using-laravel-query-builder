@extends('fontend/layouts/master')

@section('title')
Products index
@endsection

@section('bodyContent')
<div class="container my-4">
    <div class="card">
        <div class="card-header">
            <a href="{{url ('create-Product')}}"><button style="float: right" class="btn btn-success">Add
                    Product</button></a>
            <h2>All Product
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">SL</th>
                        <th scope="col">Product Name</th>
                        <th scope="col">Title</th>
                        <th scope="col">Price</th>
                        <th scope="col">Cat.. Name</th>
                        <th scope="col">SubCat.. Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>



                <tbody>

                    @foreach ($product as $item)
                    {{-- @php
                        $sl = 0
                    @endphp
                    <tr> --}}
                    <tr>
                        <td>{{ $item->id}}</td>
                        <td>{{ $item->product_name}}</td>
                        <td>{{ $item->title}}</td>
                        <td>{{ $item->price}}</td>
                        <td>{{$item->category_name}}</td>
                        <td>{{$item->subcategory_name}}</td>
                        <td>{{$item->description}}</td>
                        <td class='mx-2'>
                            <a href="{{url ('view-Product/'.$item->id)}}"><button
                                    class="btn btn-success">View</button></a>
                            <a href="{{url ('edit-Product/'.$item->id)}}"><button
                                    class="btn btn-primary">Edit</button></a>
                            <a href="{{url ('delete-Product/'.$item->id)}}"><button
                                    class="btn btn-danger">Delete</button></a>
                        </td>
                    </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection