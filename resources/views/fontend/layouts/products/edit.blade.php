@extends('fontend/layouts/master')

@section('title')
Product Edit
@endsection

@section('bodyContent')
<div class="container my-4">
    <div class="card">
        <div class="card-header">
            <a href="{{url('index-Product')}}"><button type="button" style="float: right"
                    class="btn btn-success">All Product</button></a>
            <h2>Edit Product</h2>
        </div>
        <div class="card-body">
            <form method="POST" , action="{{url('update-Product/'.$edit->id)}}">
                @csrf
                <div class="mb-3">
                    <label for="product_name" class="form-label">Product Name</label>
                    <input type="text" value="{{$edit->product_name}}" class="form-control" id="product_name"
                        name="product_name" aria-describedby="product_name">
                </div>

                <div class="mb-3">
                    <label for="title" class="form-label">Title</label>
                    <input type="text" value="{{$edit->title}}" class="form-control" id="title" name="title"
                        aria-describedby="title">
                </div>

                <div class="mb-3">
                    <label for="price" class="form-label">Price</label>
                    <input type="text" value="{{$edit->price}}" class="form-control" id="price" name="price"
                        aria-describedby="price">
                </div>

                <div class="mb-3">
                    <select class="form-select" aria-label="Default select example" name="cat_id">
                        @foreach ($category as $item)
                        @if ($item->id == $edit->cat_id)
                        <option selected value="{{$item->id}}">{{$item->category_name}}</option>
                        @else
                        <option value="{{$item->id}}">{{$item->category_name}}</option>
                        @endif
                        @endforeach

                    </select>
                </div>

                <div class="mb-3">
                    <select class="form-select" aria-label="Default select example" name="subcat_id">
                        @foreach ($subcategory as $items)
                        @if ($items->id == $edit->subcat_id)
                        <option selected value="{{$items->id}}">{{$items->subcategory_name}}</option>
                        @else
                        <option value="{{$items->id}}">{{$items->subcategory_name}}</option>
                        @endif
                        @endforeach

                    </select>
                </div>

                <div class="mb-3">
                    <label for="description" class="form-label">Description</label>
                    <input type="text" value="{{$edit->description}}" class="form-control" id="description"
                        name="description" aria-describedby="description">
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
    @endsection