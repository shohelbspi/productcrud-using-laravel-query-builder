@extends('fontend/layouts/master')

@section('title')
SubCategoty Create
@endsection


@section('bodyContent')
<div class="container my-4">
    <div class="card">
        <div class="card-header">
            <a href="{{url('index-subcategory')}}"><button  type="button" style="float: right" class="btn btn-success">All SubCategory</button></a>
            <h2>Create Sub SUCategory</h2>
        </div>
        <div class="card-body">
            <form method="POST", action="{{url('store-subcategory')}}">
                @csrf
                <div class="mb-3">
                    <label for="subcategory_name" class="form-label">Sub Category Name</label>
                    <input type="text" class="form-control" id="subcategory_name" name="subcategory_name"
                        aria-describedby="SubCategoryName">
                </div>

                <div class="mb-3">
                    <select class="form-select" aria-label="Default select example" name="cat_id">
                        @foreach ($data as $item)
                            
                        <option value="{{$item->id}}">{{$item->category_name}}</option>
                        @endforeach
        
                      </select>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection