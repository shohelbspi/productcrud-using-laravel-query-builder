@extends('fontend/layouts/master')

@section('title')
SubCategoty view
@endsection

@section('bodyContent')
<div class="container my-4">
    <div class="card" style="width: 30%">
        <div class="card-header">
            <a href="{{url ('index-subcategory')}}"><button style="float: right" class="btn btn-success">All Category</button></a>
            Sub Category Data
        </div>
        <div class="card-body">
            <p>Sub Category Name: {{$data->subcategory_name}}</p>
            <p>Category Name : {{$data->category_name}}</p>
        </div>
    </div>
</div>
@endsection