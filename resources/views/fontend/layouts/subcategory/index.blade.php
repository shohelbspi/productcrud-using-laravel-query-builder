@extends('fontend/layouts/master')

@section('title')
SubCategoty index
@endsection

@section('bodyContent')
<div class="container my-4">
    <div class="card">
        <div class="card-header">
            <a href="{{url ('create-subcategory')}}"><button style="float: right" class="btn btn-success">Add
                    sub Category</button></a>
            <h2>All Sub Category</h2>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">SL</th>
                        <th scope="col">Sub Category Name</th>
                        <th scope="col">Category Name</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>



                <tbody>

                    @foreach ($subcategory as $item)
                    {{-- @php
                        $sl = 0
                    @endphp
                    <tr> --}}
                    <tr>
                        <td>{{ $item->id}}</td>
                        <td>{{$item->subcategory_name}}</td>
                        <td>{{$item->category_name}}</td>
                        <td class='mx-2'>
                            <a href="{{url ('view-subcategory/'.$item->id)}}"><button
                                    class="btn btn-success">View</button></a>
                            <a href="{{url ('edit-subcategory/'.$item->id)}}"><button
                                    class="btn btn-primary">Edit</button></a>
                            <a href="{{url ('delete-subcategory/'.$item->id)}}"><button
                                    class="btn btn-danger">Delete</button></a>
                        </td>
                    </tr>

                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection