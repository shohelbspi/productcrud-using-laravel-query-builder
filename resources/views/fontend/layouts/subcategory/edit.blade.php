@extends('fontend/layouts/master')

@section('title')
SubCategoty Edit
@endsection

@section('bodyContent')
<div class="container my-4">
    <div class="card">
        <div class="card-header">
            <a href="{{url('index-subcategory')}}"><button type="button" style="float: right"
                    class="btn btn-success">All Category</button></a>
            <h2>Edit Sub Category</h2>
        </div>
        <div class="card-body">
            <form method="POST", action="{{url ('update-subcategory/'.$edit->id)}}">
                @csrf
                <div class="mb-3">
                    <label for="subcategory_name" class="form-label">Sub Category Name</label>
                    <input type="text" class="form-control" value="{{$edit->subcategory_name}}" id="subcategory_name"
                        name="subcategory_name" aria-describedby="SubCategoryName">
                </div>
                <div class="mb-3">
                    <select class="form-select" aria-label="Default select example" name="cat_id">
                        @foreach ($data as $item)

                        @if ($item->id == $edit->cat_id)

                        <option selected value="{{$item->id}}">{{$item->category_name}}</option>
                        @else
                        <option  value="{{$item->id}}">{{$item->category_name}}</option>

                        @endif
                        @endforeach

                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
    @endsection