<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SubCategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Category Route

Route::get('/index-category', [CategoryController::class, 'index'])->name('index');
Route::get('/create-category', [CategoryController::class, 'create'])->name('create');
Route::post('/store-category', [CategoryController::class, 'store'])->name('store');
Route::get('/view-category/{id}', [CategoryController::class, 'view'])->name('view');
Route::get('/edit-category/{id}',[CategoryController::class, 'edit'])->name('edit');
Route::post('/update-category/{id}',[CategoryController::class,'update'])->name('update');
Route::get('/delete-category/{id}',[CategoryController::class, 'delete'])->name('delete');

// Subcategory

Route::get('/index-subcategory', [SubCategoryController::class, 'index'])->name('index');
Route::get('/create-subcategory', [SubCategoryController::class, 'create'])->name('create');
Route::post('/store-subcategory', [SubCategoryController::class, 'store'])->name('store');
Route::get('/view-subcategory/{id}', [SubCategoryController::class, 'view'])->name('view');
Route::get('/edit-subcategory/{id}',[SubCategoryController::class, 'edit'])->name('edit');
Route::post('/update-subcategory/{id}',[SubCategoryController::class,'update'])->name('update');
Route::get('/delete-subcategory/{id}',[SubCategoryController::class, 'delete'])->name('delete');


Route::get('/index-Product', [ProductController::class, 'index'])->name('index');
Route::get('/create-Product', [ProductController::class, 'create'])->name('create');
Route::post('/store-Product', [ProductController::class, 'store'])->name('store');
Route::get('/view-Product/{id}', [ProductController::class, 'view'])->name('view');
Route::get('/edit-Product/{id}',[ProductController::class, 'edit'])->name('edit');
Route::post('/update-Product/{id}',[ProductController::class,'update'])->name('update');
Route::get('/delete-Product/{id}',[ProductController::class, 'delete'])->name('delete');